" SOLARIZED
" ---------
syntax enable
set background=dark
colorscheme solarized

" Set tabs to 4 spaces
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" Set autoindentation (Each new line will be automatically indented)
set autoindent

" Delete unnecessary spaces when saving
autocmd BufWritePre * %s/\s\+$//e

" SYNTASTIC
" ---------
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'
