syntax enable

colorscheme railscasts

set encoding=utf-8

" Delete unnecessary spaces when saving
autocmd BufWritePre * %s/\s\+$//e

" show a visual line under the cursor's current line
set cursorline

" Set tabs to 4 spaces
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" Set autoindentation (Each new line will be automatically indented)
set autoindent

" enable all Python syntax highlighting features
let python_highlight_all = 1

" show the matching part of the pair for [] {} and ()
set showmatch

" make in vim
set makeprg=python\ %
set autowrite

" SYNTASTIC
" ---------
let g:syntastic_python_checkers = ['pylint']
