set guifont=Bitstream\ Vera\ Sans\ Mono
" No menu
set guioptions-=m
" No Toolbar
set guioptions-=T
" Tabs show file name instead of the whole path
set guitablabel=%t
