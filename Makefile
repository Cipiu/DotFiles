#------------------------------------------------------------------------------#
# Makefile
# Installs the various dotfiles.
#------------------------------------------------------------------------------#

# Default target. Will be filled up as targets are declared.
all:
.PHONY: all


#------------------------------------------------------------------------------#
# symlink targets
#------------------------------------------------------------------------------#

# Creates the target $1 which symlinks the file ~/Dotfiles/$1 to ~/$2/$1
define symlink
$1:
	@-mkdir -p $(dir ~/$2/$1)
	@-rm -rf ~/$2/$1
	ln -s ~/DotFiles/$1 ~/$2/$1

.PHONY: $1 # symlinks make bad target so make them phonies.
all: $1
endef

# symlink targets
$(eval $(call symlink,.bashrc))
$(eval $(call symlink,.gitconfig))
$(eval $(call symlink,.gvimrc))
$(eval $(call symlink,.vim))
$(eval $(call symlink,.vimrc))
$(eval $(call symlink,.gdbinit))
$(eval $(call symlink,.tmux.conf))
