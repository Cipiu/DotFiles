" PATHOGEN
" --------
execute pathogen#infect()
syntax on
filetype plugin indent on

" NERD TREE
" ---------
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
map <C-n> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1
let NERDTreeShowHidden=1

" SYNTASTIC
" ---------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
nnoremap <F3> :SyntasticToggleMode<CR>
nnoremap <F4> :SyntasticCheck<CR>

" AIRLINE
" -------
" Always show the status line
set laststatus=2

" OTHER SETTINGS
" --------------
set t_Co=256
set background=dark
colorscheme monokai

" Set searched word highlight
set hlsearch

" Set incremental search (The Vim editor will start searching when you type
" the first character of the search string)
set incsearch

" Enable mouse
set mouse=a

" Show line number
set nu
