################################################################################
# Random tweaks that makes my gdb life easier.
################################################################################

source ~/dotfiles/stl-views.gdb


#
# C++ related beautifiers
#

set print pretty on
set print object on
set print static-members on
set print vtbl on
set print demangle on
set print asm-demangle on
set demangle-style gnu-v3
set print sevenbit-strings off

#
# Utilities
#

define run_and_exit
    catch throw
    python gdb.events.exited.connect(lambda x : gdb.execute("quit"))
    run
end

document run_and_exit
Runs the program and exits if no error occur.
end
